#[cfg(feature = "sqlite")]
use easy_sqlite::errors::DbError;

#[derive(Debug, PartialEq)]
pub enum AtexError {
    Db(String),
    TaskQueueIsBroken,
    TaskJoinMechanismFailed,
    ExecutorNotFound,
    GetFromEmptyCell,
    PutInNonEmptyCell,
    InvalidExecutor,
    InvalidName,
    InvalidId,
    LockIsAlreadyInUse,
}

#[cfg(feature = "sqlite")]
impl From<DbError> for AtexError {
    fn from(value: DbError) -> Self {
        Self::Db(format!("{:?}", value))
    }
}
