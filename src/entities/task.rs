use std::fmt::Debug;

#[derive(Clone, Debug)]
pub enum TaskStatus {
    Error(String),
    InProgress(u8),
    Completed,
}

pub type TaskId = u64;

#[derive(Clone)]
pub struct Task {
    pub id: TaskId,
    pub periodic_interval: Option<u64>,
    pub executor: &'static str,
    pub status: TaskStatus,
    pub payload: String,
    pub created: u64,
    pub updated: u64,
    pub execute_after: u64,
    pub lock: String,
}

pub struct NewRepoTask {
    pub(crate) periodic_interval: Option<u64>,
    pub(crate) executor: &'static str,
    pub(crate) payload: String,
    pub(crate) created: u64,
    pub(crate) updated: u64,
    pub(crate) execute_after: u64,
    pub(crate) lock: String,
}

#[derive(Clone, Debug, PartialEq)]
pub struct NewTask {
    pub executor: &'static str,
    pub payload: String,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct NewPeriodic {
    pub(crate) executor: &'static str,
    pub(crate) interval: u64,
}

pub(crate) struct TaskStarted {
    pub executor: &'static str,
    pub started: u64,
}

#[derive(Debug)]
pub struct TaskStats {
    pub executor: &'static str,
    pub active_for: u64,
}

impl<R, E: Debug> From<Result<R, E>> for TaskStatus {
    fn from(value: Result<R, E>) -> Self {
        match value {
            Ok(_) => TaskStatus::Completed,
            Err(err) => TaskStatus::Error(format!("{:?}", err)),
        }
    }
}
