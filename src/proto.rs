use std::pin::Pin;

use crate::entities::error::AtexError;
use crate::entities::task::{NewRepoTask, NewTask, Task, TaskId, TaskStats, TaskStatus};

pub(crate) type DynFut<T> = Pin<Box<dyn std::future::Future<Output = T> + Send + 'static>>;

pub trait ITaskController: Send + Sync {
    fn set_progress(&self, id: TaskId, progress: u8) -> DynFut<Result<(), AtexError>>;
    fn create_task(&self, task: NewTask) -> DynFut<Result<TaskId, AtexError>>;
}

pub trait ITaskManager {
    fn create_task(&self, task: NewTask) -> DynFut<Result<TaskId, AtexError>>;
    fn schedule_periodic_now(&self, name: &str) -> DynFut<Result<(), AtexError>>;
    fn manual_execute_periodic(&self, name: &str) -> DynFut<Result<TaskStatus, AtexError>>;
    fn manual_execute_task(&self, task_id: TaskId) -> DynFut<Result<TaskStatus, AtexError>>;
    fn get(&self, id: TaskId) -> DynFut<Result<Option<Task>, AtexError>>;
    fn run(&self) -> DynFut<Result<(), AtexError>>;
    fn get_stats(&self) -> DynFut<Vec<Option<TaskStats>>>;
}

pub trait ITaskRepo: Send + Sync {
    fn init(&mut self) -> Result<(), AtexError>;
    fn create(&mut self, task: NewRepoTask) -> Result<TaskId, AtexError>;
    fn get_by_id(&self, id: TaskId) -> Result<Option<Task>, AtexError>;
    fn get_by_executor(&self, executor: &str) -> Result<Option<Task>, AtexError>;
    fn get_ready(&self, exclude: &[TaskId], limit: usize) -> Result<Vec<Task>, AtexError>;
    fn set_status(&mut self, id: TaskId, task_status: TaskStatus) -> Result<(), AtexError>;
    fn set_progress(&mut self, id: TaskId, progress: u8) -> Result<(), AtexError>;
    fn set_execute_after(&mut self, id: TaskId, ts: u64) -> Result<(), AtexError>;
    fn delete_completed_tasks(&mut self, max_lifetime: u64) -> Result<(), AtexError>;
    fn delete_error_tasks(&mut self, max_lifetime: u64) -> Result<(), AtexError>;
}

pub(crate) trait IPool<K, V>: Send + Sync {
    fn len(&self) -> usize;
    fn free_cells(&self) -> Vec<usize>;
    fn get_keys(&self) -> Vec<K>;
    fn get_cell(&mut self, id: usize) -> Result<(K, V), AtexError>;
    fn put_cell(&mut self, id: usize, key: K, val: V) -> Result<(), AtexError>;
}

pub(crate) trait IPoolFactory<K, V>: Send + Sync {
    type Pool: IPool<K, V>;
    fn len(&self) -> usize;
    fn produce(&self) -> Self::Pool;
}

pub trait ILogger: Send + Sync {
    fn log(&self, msg: &str);
}

pub trait ITaskExecutor: Send + Sync {
    type Error: std::fmt::Debug;

    fn name(&self) -> &'static str;
    fn periodic_interval(&self) -> Option<u64> {
        None
    }
    fn lock_key(&self, payload: &str) -> String {
        format!("{}:{}", self.name(), payload)
    }
    fn execute(
        &self,
        ctrl: Box<dyn ITaskController>,
        task: Task,
    ) -> DynFut<Result<TaskStatus, Self::Error>>;
}

impl<E: ITaskExecutor> ITaskExecutor for &E {
    type Error = E::Error;

    fn name(&self) -> &'static str {
        E::name(self)
    }

    fn periodic_interval(&self) -> Option<u64> {
        E::periodic_interval(self)
    }

    fn execute(
        &self,
        ctrl: Box<dyn ITaskController>,
        task: Task,
    ) -> DynFut<Result<TaskStatus, Self::Error>> {
        E::execute(self, ctrl, task)
    }
}

pub(crate) trait IWrappedTaskExecutor: Send + Sync {
    fn name(&self) -> &'static str;
    fn periodic_interval(&self) -> Option<u64>;
    fn lock_key(&self, payload: &str) -> String;
    fn execute(&self, ctrl: Box<dyn ITaskController>, task: Task) -> DynFut<TaskStatus>;
}
