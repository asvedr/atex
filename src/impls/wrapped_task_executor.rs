use crate::proto::{DynFut, ILogger, IWrappedTaskExecutor};
use crate::{ITaskController, ITaskExecutor, Task, TaskStatus};
use std::sync::Arc;

pub struct WrappedTaskExecutor<L: ILogger, E: ITaskExecutor> {
    logger: Arc<L>,
    executor: E,
}

impl<L: ILogger, E: ITaskExecutor> WrappedTaskExecutor<L, E> {
    pub fn new(logger: Arc<L>, executor: E) -> Self {
        Self { logger, executor }
    }
}

impl<L: ILogger + 'static, E: ITaskExecutor + 'static> IWrappedTaskExecutor
    for WrappedTaskExecutor<L, E>
{
    fn name(&self) -> &'static str {
        self.executor.name()
    }

    fn periodic_interval(&self) -> Option<u64> {
        self.executor.periodic_interval()
    }

    fn lock_key(&self, payload: &str) -> String {
        self.executor.lock_key(payload)
    }

    fn execute(&self, ctrl: Box<dyn ITaskController>, task: Task) -> DynFut<TaskStatus> {
        let fut = self.executor.execute(ctrl, task);
        let logger = self.logger.clone();
        Box::pin(async move {
            match fut.await {
                Ok(status) => status,
                Err(err) => {
                    let msg = format!("{:?}", err);
                    logger.log(&msg);
                    TaskStatus::Error(msg)
                }
            }
        })
    }
}
