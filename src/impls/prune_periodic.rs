use crate::entities::error::AtexError;
use crate::{ITaskController, ITaskExecutor, Task, TaskStatus};
use std::sync::Arc;
use tokio::sync::Mutex;

use crate::proto::{DynFut, ITaskRepo};

pub(crate) struct PrunePeriodic<R: ITaskRepo> {
    pub(crate) repo: Arc<Mutex<R>>,
    pub(crate) completed_lifetime: u64,
    pub(crate) err_lifetime: u64,
}

async fn a_execute<R: ITaskRepo>(
    mtx: Arc<Mutex<R>>,
    completed_lifetime: u64,
    err_lifetime: u64,
) -> Result<TaskStatus, AtexError> {
    let mut repo = mtx.lock().await;
    repo.delete_completed_tasks(completed_lifetime)?;
    repo.delete_error_tasks(err_lifetime)?;
    Ok(TaskStatus::Completed)
}

impl<R: ITaskRepo + 'static> ITaskExecutor for PrunePeriodic<R> {
    type Error = AtexError;

    fn name(&self) -> &'static str {
        "__std_prune"
    }

    fn periodic_interval(&self) -> Option<u64> {
        let time = self.completed_lifetime.min(self.err_lifetime);
        Some(time)
    }

    fn execute(
        &self,
        _: Box<dyn ITaskController>,
        _: Task,
    ) -> DynFut<Result<TaskStatus, AtexError>> {
        let repo = self.repo.clone();
        let completed = self.completed_lifetime;
        let err = self.err_lifetime;
        Box::pin(a_execute(repo, completed, err))
    }
}
