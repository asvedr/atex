use crate::proto::ILogger;

pub struct FuncLogger {
    pub(crate) func: Box<dyn Fn(&str)>,
}

unsafe impl Send for FuncLogger {}
unsafe impl Sync for FuncLogger {}

impl ILogger for FuncLogger {
    fn log(&self, msg: &str) {
        (self.func)(msg)
    }
}
