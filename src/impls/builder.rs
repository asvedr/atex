#[cfg(feature = "sqlite")]
use easy_sqlite::RSQLConnection;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Mutex;

use crate::entities::error::AtexError;
use crate::impls::func_logger::FuncLogger;
#[cfg(feature = "in_memory")]
use crate::impls::in_mem::InMemRepo;
use crate::impls::pool::PoolFactory;
use crate::impls::prune_periodic::PrunePeriodic;
#[cfg(feature = "sqlite")]
use crate::impls::sqlite::repo::DbTaskRepo;
use crate::impls::stderr_logger::MockLogger;

use crate::impls::task_manager::TaskManager;
use crate::impls::wrapped_task_executor::WrappedTaskExecutor;
use crate::proto::{ILogger, ITaskExecutor, ITaskManager, ITaskRepo, IWrappedTaskExecutor};

const SLEEP_ON_EMPTY_CHANNEL: Duration = Duration::from_secs(2);
const POOL_SIZE: usize = 10;
const SUCC_TASK_LIFETIME: u64 = 60 * 60 * 60;
const ERR_TASK_LIFETIME: u64 = 60 * 60 * 60;

pub struct TaskManagerBuilder;

pub struct TaskManagerBuilderCtx<R: ITaskRepo, L: ILogger> {
    executors: HashMap<&'static str, Box<dyn IWrappedTaskExecutor>>,
    pool_size: usize,
    succ_task_lifetime: u64,
    err_task_lifetime: u64,
    repo_maker: Box<dyn Fn() -> Result<R, AtexError>>,
    logger: Arc<L>,
    sleep: Duration,
}

impl TaskManagerBuilder {
    #[cfg(feature = "sqlite")]
    pub fn new_sqlite(
        db_path: &str,
    ) -> TaskManagerBuilderCtx<DbTaskRepo<RSQLConnection>, MockLogger> {
        let db_path = db_path.to_string();
        let repo_maker = Box::new(move || -> Result<DbTaskRepo<RSQLConnection>, AtexError> {
            let ex = RSQLConnection::new(db_path.clone())?;
            Ok(DbTaskRepo { ex })
        });
        TaskManagerBuilderCtx {
            executors: Default::default(),
            pool_size: POOL_SIZE,
            succ_task_lifetime: SUCC_TASK_LIFETIME,
            err_task_lifetime: ERR_TASK_LIFETIME,
            repo_maker,
            logger: Arc::new(MockLogger),
            sleep: SLEEP_ON_EMPTY_CHANNEL,
        }
    }

    #[cfg(feature = "in_memory")]
    pub fn new_mem() -> TaskManagerBuilderCtx<InMemRepo, MockLogger> {
        let repo_maker = Box::new(|| Ok(InMemRepo::default()));
        TaskManagerBuilderCtx {
            executors: Default::default(),
            pool_size: POOL_SIZE,
            succ_task_lifetime: SUCC_TASK_LIFETIME,
            err_task_lifetime: ERR_TASK_LIFETIME,
            repo_maker,
            logger: Arc::new(MockLogger),
            sleep: SLEEP_ON_EMPTY_CHANNEL,
        }
    }
}

impl<R: ITaskRepo + 'static, L: ILogger + 'static> TaskManagerBuilderCtx<R, L> {
    pub fn add_executor(mut self, ex: impl ITaskExecutor + 'static) -> Self {
        let name = ex.name();
        let wrapped = WrappedTaskExecutor::new(self.logger.clone(), ex);
        self.executors.insert(name, Box::new(wrapped));
        self
    }

    pub fn set_pool_size(mut self, pool_size: usize) -> Self {
        self.pool_size = pool_size;
        self
    }

    pub fn set_sleep(mut self, sleep: Duration) -> Self {
        self.sleep = sleep;
        self
    }

    pub fn set_succ_task_lifetime(mut self, lt: u64) -> Self {
        self.succ_task_lifetime = lt;
        self
    }

    pub fn set_err_task_lifetime(mut self, lt: u64) -> Self {
        self.err_task_lifetime = lt;
        self
    }

    pub fn set_log_func(self, func: Box<dyn Fn(&str)>) -> TaskManagerBuilderCtx<R, FuncLogger> {
        TaskManagerBuilderCtx {
            executors: self.executors,
            pool_size: self.pool_size,
            succ_task_lifetime: self.succ_task_lifetime,
            err_task_lifetime: self.err_task_lifetime,
            repo_maker: self.repo_maker,
            logger: Arc::new(FuncLogger { func }),
            sleep: self.sleep,
        }
    }

    pub fn build(self) -> Result<Box<dyn ITaskManager>, AtexError> {
        let mut repo = (self.repo_maker)()?;
        repo.init()?;
        let repo = Arc::new(Mutex::new(repo));
        let prune = PrunePeriodic {
            repo: repo.clone(),
            completed_lifetime: self.succ_task_lifetime,
            err_lifetime: self.err_task_lifetime,
        };
        let prune = Box::new(WrappedTaskExecutor::new(self.logger.clone(), prune));
        let mut executors = self.executors;
        executors.insert(prune.name(), prune);
        let pool_factory = PoolFactory::new(self.pool_size);
        let man = TaskManager::new(executors, pool_factory, repo, self.logger, self.sleep);
        Ok(Box::new(man))
    }
}
