use crate::proto::ILogger;

pub struct MockLogger;

impl ILogger for MockLogger {
    fn log(&self, msg: &str) {
        eprintln!("{}", msg);
    }
}
