use easy_sqlite::{ITable, SStr, SVec};

pub(crate) struct DbTask;

impl DbTask {
    pub const STATUS_ERR: i8 = -1;
    pub const STATUS_PROGRESS: i8 = 0;
    pub const STATUS_COMPLETED: i8 = 1;
    pub const MIN_PROGRESS: i8 = 0;
    pub const MAX_PROGRESS: i8 = 99;
}

impl ITable for DbTask {
    const NAME: SStr = "atex_task";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("periodic_interval", "INTEGER"),
        ("executor", "TEXT NOT NULL"),
        ("error", "TEXT"),
        ("status", "INTEGER NOT NULL"),
        ("progress", "INTEGER"),
        ("payload", "TEXT"),
        ("created", "INTEGER"),
        ("updated", "INTEGER"),
        ("execute_after", "INTEGER"),
        ("lock", "STRING"),
    ];
    const INDEXES: SVec<SStr> = &["status", "periodic_interval", "execute_after"];
}
